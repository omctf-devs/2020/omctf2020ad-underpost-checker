import connexion

app = connexion.App(__name__, specification_dir='api/')
app.add_api('config.yml', base_path='/v1')
app.run(host='0.0.0.0', port='8080', debug=False)
