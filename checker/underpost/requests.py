import json

from settings import (
    TIMEOUT
)
from underpost.api_urls import (
    get_url,
)
from underpost.utils import (
    print_request,
    handle_response,
)


def registration(session, hostname, username, password, extras):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ',
    }
    data = json.dumps({
        'login': username,
        'password': password,
        'question': extras.get('question'),
        'answer': extras.get('answer'),
        'contactNumber': extras.get('contactNumber'),
        'personalID': extras.get('personalID'),
    })
    url = get_url(hostname, 'registration')

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Registration')

    response_body = response.json()

    return response_body.get('token'), response_body


def login(session, hostname, username, password):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ',
    }
    data = json.dumps({
        'login': username,
        'password': password,
    })
    url = get_url(hostname, 'login')

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Authorization')

    response_body = response.json()

    return response_body.get('token'), response_body


def get_couriers(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'couriers')

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get couriers')

    return response.json()


def get_goods(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'goods')

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get goods')

    return response.json()


def get_bunkers(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'bunkers')

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get bunkers')

    return response.json()


def create_order(session, hostname, token, good_id, target_id, comment):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'orders')
    data = json.dumps({
        'goodId': good_id,
        'targetId': target_id,
        'comment': comment,
    })

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Create order')

    return response.json()


def get_orders(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'orders')

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get orders')

    return response.json()


def complain_courier(session, hostname, token, courier_id, complaint):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'appeal_courier') + '/%s' % courier_id
    data = json.dumps({
        'complaint': complaint,
    })

    response = session.post(
        url,
        headers=headers,
        data=data,
        timeout=TIMEOUT
    )

    print_request('POST', response.url, headers, data)
    handle_response(response, 'Complain courier')

    return response.json()


def get_courier_complaints(session, hostname, token):
    headers = {
        'Authorization': 'Bearer ' + token,
    }
    url = get_url(hostname, 'appeal_courier')

    response = session.get(
        url,
        headers=headers,
        timeout=TIMEOUT
    )

    print_request('GET', response.url, headers)
    handle_response(response, 'Get courier complains')

    return response.json()
