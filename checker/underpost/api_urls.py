BASE_URL = 'http://%s/api'

url_map = {
    'login': BASE_URL + '/session',
    'registration': BASE_URL + '/users',
    'couriers': BASE_URL + '/couriers',
    'goods': BASE_URL + '/goods',
    'bunkers': BASE_URL + '/bunkers',
    'orders': BASE_URL + '/orders',
    'appeal_courier': BASE_URL + '/appeals/couriers',
}


def get_url(host, endpoint):
    return url_map[endpoint] % host
