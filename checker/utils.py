import sys
from sys import exc_info
import random
import string
from hashlib import md5
from traceback import format_exception


def rand_str():
    n = int(random.uniform(10, 50))
    a = string.ascii_letters + string.digits
    return str(''.join([random.choice(a) for i in range(n)]))


def string2numeric_hash(text, encoding='utf-8'):
    return int(md5(text.encode(encoding)).hexdigest()[:8], 16)


def traceback_to_string():
    e_type, value, tb = sys.exc_info()
    return ''.join(format_exception(e_type, value, tb, None))


class RestException(Exception):
    pass


class InconsistentException(Exception):
    pass
