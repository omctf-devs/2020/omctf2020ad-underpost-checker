import sys
from sys import exc_info
import base64

import requests.utils

from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)
from underpost.requests import (
    login,
    get_orders,
    get_courier_complaints,
)
from utils import (
    traceback_to_string,
    RestException,
    InconsistentException,
)


def get_flag(host, uid, flag, vuln):
    # check that flag exists in certain vulnerability
    try:
        with requests.Session() as s:
            if vuln == 1:
                return get_first_vuln_flag(s, host, uid, flag)
            elif vuln == 2:
                return get_second_vuln_flag(s, host, uid, flag)
            elif vuln == 3:
                return get_third_vuln_flag(s, host, uid, flag)
            elif vuln == 4:
                return get_fourth_vuln_flag(s, host, uid, flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return Result(ERROR, ('Invalid vulnerability index: %s' % vuln))
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        print('unknown err', exc_info())
        return Result(ERROR, 'Unknown error', traceback_to_string())


def get_credentials(uid):
    uid_bytes = str.encode(uid, 'utf-8')
    cred_bytes = base64.b64decode(uid_bytes)
    cred_str = cred_bytes.decode('utf-8')

    creds = cred_str.split(':')
    if len(creds) < 2:
        raise InconsistentException("Invalid uid")
    return creds[0], creds[1]


def get_first_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    _, auth_data = login(session, host, username, password)

    user_specific = auth_data['data']
    personal_id = user_specific['personalID']

    if personal_id == flag:
        return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')


def get_second_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    token, _ = login(session, host, username, password)
    orders = get_orders(session, host, token)

    if len(orders) > 0 and orders[0]['comment'] == flag:
        return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')


def get_third_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    token, _ = login(session, host, username, password)
    complaints = get_courier_complaints(session, host, token)

    if len(complaints) > 0 and complaints[0]['complaint'] == flag:
        return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')


def get_fourth_vuln_flag(session, host, uid, flag):
    username, password = get_credentials(uid)
    _, auth_data = login(session, host, username, password)

    user_specific = auth_data['data']
    personal_id = user_specific['personalID']

    if personal_id == flag:
        return Result(SUCCESS, 'flag is still here')

    return Result(CORRUPT, 'flag is corrupted')
