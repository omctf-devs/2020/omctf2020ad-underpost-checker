import sys
from sys import exc_info

import requests.utils
from faker import Faker

from methods.result import (
    Result,
    SUCCESS,
    MUMBLE,
    DOWN,
    ERROR,
)
from underpost.requests import (
    registration,
    login,
    get_couriers,
    get_goods,
    get_bunkers,
    create_order,
    get_orders,
)
from utils import (
    rand_str,
    string2numeric_hash,
    traceback_to_string,
    RestException,
    InconsistentException,
)

fake = Faker()


def health_check(host):
    # check some service endpoints to make sure it works correctly
    try:
        with requests.Session() as s:
            username = fake.user_name()
            password = string2numeric_hash(rand_str())
            user_extras = {
                'question': 'What is my name?',
                'answer': fake.name(),
                'contactNumber': '123-123',
                'personalID': username,
            }
            registration(s, host, username, password, user_extras)
            token, _ = login(s, host, username, password)
            couriers = get_couriers(s, host, token)
            goods = get_goods(s, host, token)
            bunkers = get_bunkers(s, host, token)

            order = create_order(
                s, host, token,
                good_id=goods[0]['id'],
                target_id=bunkers[0]['id'],
                comment=fake.text(),
            )

            orders = get_orders(s, host, token)
            if len(orders) == 1 and order in orders:
                return Result(SUCCESS, "It's alive!")
            else:
                raise InconsistentException("Test user created during check should have one associated specific order")

    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API error', str(re))
    except InconsistentException as inc:
        print(inc, file=sys.stderr)
        return Result(MUMBLE, 'Service API returned inconsistent data', str(inc))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        return Result(ERROR, 'Unknown error', traceback_to_string())
