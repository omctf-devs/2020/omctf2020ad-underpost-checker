import sys
from sys import exc_info
import base64
import random

import requests.utils
from faker import Faker

from underpost.requests import (
    registration,
    get_goods,
    get_bunkers,
    create_order,
    get_couriers,
    complain_courier,
)
from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)
from utils import (
    rand_str,
    string2numeric_hash,
    traceback_to_string,
    RestException,
)

fake = Faker()


def put_flag(host, flag, vuln):
    # put flags in different vulnerabilities (uid not used, because it return own id, which consists of login:password)
    vuln = int(vuln)
    if not (1 <= vuln <= 4):
        print("Invalid vuln", file=sys.stderr)
        return ERROR

    try:
        with requests.Session() as s:
            if vuln == 1:
                return put_first_vuln_flag(s, host, flag)
            elif vuln == 2:
                return put_second_vuln_flag(s, host, flag)
            elif vuln == 3:
                return put_third_vuln_flag(s, host, flag)
            elif vuln == 4:
                return put_fourth_vuln_flag(s, host,flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return Result(ERROR, ('Invalid vulnerability index: %s' % vuln))
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        print('unknown err', exc_info())
        return Result(ERROR, 'Unknown error', traceback_to_string())


def generate_uid(username, password):
    cred_str = '%s:%s' % (username, password)
    uid_bytes = str.encode(cred_str, 'utf-8')
    uid_base64_bytes = base64.encodebytes(uid_bytes)
    uid = uid_base64_bytes.decode('utf-8')

    return uid


def put_first_vuln_flag(session, host, flag):
    username = 'omctfSamStrand{rand}'.format(rand=random.randint(1111, 9999))
    password = string2numeric_hash(rand_str())
    user_extras = {
        'question': 'What is my full name?',
        'answer': 'Sam Strand',
        'contactNumber': '123-123',
        'personalID': flag,
    }
    registration(session, host, username, password, user_extras)

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)


def put_second_vuln_flag(session, host, flag):
    username = fake.user_name()
    password = string2numeric_hash(rand_str())
    user_extras = {
        'question': 'What is my name?',
        'answer': fake.name(),
        'contactNumber': '123-123',
        'personalID': username,
    }
    token, _ = registration(session, host, username, password, user_extras)

    goods = get_goods(session, host, token)
    bunkers = get_bunkers(session, host, token)

    create_order(
        session, host, token,
        good_id=goods[0]['id'],
        target_id=bunkers[0]['id'],
        comment=flag,
    )

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)


def put_third_vuln_flag(session, host, flag):
    username = fake.user_name()
    password = string2numeric_hash(rand_str())
    user_extras = {
        'question': 'What is my name?',
        'answer': fake.name(),
        'contactNumber': '123-123',
        'personalID': username,
    }
    token, _ = registration(session, host, username, password, user_extras)

    couriers = get_couriers(session, host, token)
    couriers = [c for c in couriers if c['id']]

    print(couriers)

    courier = couriers[0]

    complain_courier(session, host, token, courier['id'], flag)

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)


def put_fourth_vuln_flag(session, host, flag):
    username = fake.user_name()
    password = string2numeric_hash(rand_str())
    user_extras = {
        'question': 'What is my name?',
        'answer': fake.name(),
        'contactNumber': '123-123',
        'personalID': flag,
    }
    registration(session, host, username, password, user_extras)

    uid = generate_uid(username, password)
    print('uid: %s' % uid)

    return Result(SUCCESS, extra_data=uid)





