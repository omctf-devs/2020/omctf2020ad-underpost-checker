## Docker

### Build Image

Run `docker-compose build` from the project's root.

### Run Checker Service in Docker container locally

Run `docker-compose up` from the project's root.

### Swagger

Available at: `http://localhost:8080/v1/ui/`.

### Notes

If you run `underpost-api` service using docker-compose, the `host` parameter for checker will be `192.168.60.2:8078`.

IP can be changed in `underpost-api`'s docker-compose `ipv4_address` section.

Port can be changed in `underpost-api`'s Dockerfile `EXPOSE` section.

## Virtualenv

Comingsoon :)
