FROM python:3.7

COPY /checker /underpost/checker
COPY requirements.txt /underpost/requirements.txt

WORKDIR /underpost

RUN pip install -r requirements.txt

EXPOSE 8080

ENTRYPOINT ["python"]
CMD ["-u", "checker/service.py"]
